echo "<< Executing Deploy Script now >>"

echo "<< Copying buildjar to WT_HOME and Stopping Servers >>"

$propsFile = "build.properties"

# Check if property file exists

$buildProps = Get-Content $propsFile | Out-String | ConvertFrom-StringData

# Check if required properties are available in the property file


$wtHome =  $buildProps.wtHome
$jarName = $buildProps.{build.jar.name} + $buildProps.{build.number} + ".jar"
$jarPath = $buildProps.{build.jar.target.location} + $jarName
$fileContent = Get-Content $jarPath -Raw
$targetJatPath = $wtHome+"/"+$jarName

$username="carters\flexplmstage"
$PWord = ConvertTo-SecureString -String "Kuc93L72Dp" -AsPlainText -Force
$cred = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $username, $PWord

$session1 = New-PSSession -Credential $cred -Authentication Default -ComputerName WSSPLM26.carters.com
Invoke-Command -Session $session1 -ScriptBlock {Set-Content -Path $using:targetJatPath -value $using:fileContent}
Invoke-Command -Session $session1 -ErrorAction Continue -ScriptBlock {& cmd.exe /c "E:\ptc\Windchill_11.0\Windchill\bin\windchill.exe stop"}
Remove-PSSession $session1


$session2 = New-PSSession -Credential $cred -Authentication Default -ComputerName WSSPLM25.carters.com
Invoke-Command -Session $session2 -ScriptBlock {Set-Content -Path $using:targetJatPath -value $using:fileContent}
Invoke-Command -Session $session2 -ErrorAction Continue -ScriptBlock {& cmd.exe /c "E:\ptc\Windchill_11.0\Windchill\bin\windchill.exe stop"}
Remove-PSSession $session2


$session3 = New-PSSession -Credential $cred -Authentication Default -ComputerName WSSPLM24.carters.com
Invoke-Command -Session $session3 -ScriptBlock {Set-Content -Path $using:targetJatPath -value $using:fileContent}
Invoke-Command -Session $session3 -ErrorAction Continue -ScriptBlock {& cmd.exe /c "E:\ptc\Windchill_11.0\Windchill\bin\windchill.exe stop"}
Remove-PSSession $session3


$session4 = New-PSSession -Credential $cred -Authentication Default -ComputerName WSSPLM23.carters.com
Invoke-Command -Session $session4 -ScriptBlock {Set-Content -Path $using:targetJatPath -value $using:fileContent}
Invoke-Command -Session $session4 -ErrorAction Continue -ScriptBlock {& cmd.exe /c "E:\ptc\Windchill_11.0\Windchill\bin\windchill.exe stop"}
Remove-PSSession $session4


$session5 = New-PSSession -Credential $cred -Authentication Default -ComputerName WSSPLM22.carters.com
Invoke-Command -Session $session5 -ScriptBlock {Set-Content -Path $using:targetJatPath -value $using:fileContent}
Invoke-Command -Session $session5 -ErrorAction Continue -ScriptBlock {& cmd.exe /c "E:\ptc\Windchill_11.0\Windchill\bin\windchill.exe stop"}
Remove-PSSession $session5


$session6 = New-PSSession -Credential $cred -Authentication Default -ComputerName WSSPLM21.carters.com
Invoke-Command -Session $session6 -ScriptBlock {Set-Content -Path $using:targetJatPath -value $using:fileContent}
Invoke-Command -Session $session6 -ErrorAction Continue -ScriptBlock {& cmd.exe /c "E:\ptc\Windchill_11.0\Windchill\bin\windchill.exe stop"}
Remove-PSSession $session6

