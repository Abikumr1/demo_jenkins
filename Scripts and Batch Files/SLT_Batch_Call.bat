echo off

C:
REM go to the WT_HOME directory
cd E:\ptc\Windchill_11.0\Windchill

REM switch to the bin directory
pushd E:\ptc\Windchill_11.0\Windchill\bin

windchill com.wcc.wc.reports.StyleTrackBatchProcess
pause
