﻿echo "<< Executing Deploy Script now >>"

echo "<< Copying buildjar to WT_HOME >>"

$propsFile = "build.properties"

# Check if property file exists

$buildProps = Get-Content $propsFile | Out-String | ConvertFrom-StringData

# Check if required properties are available in the property file


$wtHome =  $buildProps.wtHome
$jarName = $buildProps.{build.jar.name} + $buildProps.{build.number} + ".jar"
$jarPath = $buildProps.{build.jar.target.location} + $jarName

#cp $jarPath $wtHome
cd $wtHome

echo "<< Extracting jar >>"
jar -xvf $jarName

cd bin

echo "<< Restarting Servers >>"


echo "<< Stopping Windchill >>"

try
{
            
    #./windchill stop
    
    echo "<< Clearing Windchill Cache Folders >>"
    rm -R -Force $wtHome\tomcat\instances\*
    rm -R -Force $wtHome\tasks\codebase\com\infoengine\compiledTasks\file\*
    
    echo "<< Starting Windchill >>"
    ./windchill start

    #echo "<< Waiting for Windchill to start ... >>"
    #Start-Sleep -s 180
    #echo "<< Continue >>"
}
catch
{
    echo "<< ERROR while running Restart Servers script >>"
    Return $Error[0].Exception
}


